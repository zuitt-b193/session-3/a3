package com.zuitt.batch193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Activity {

    public static void main(String[] args) {

        //scanner
        Scanner input = new Scanner(System.in);

        //declaring and initializing variables
        int num = 1;
        int i = 1;
        int number = 1;

        try {
            //input a number
            System.out.println("Input an integer whose factorial will be computed");
            number = input.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Input is not an integer");
        } catch (Exception e) {
            System.out.println("Invalid input");
        } finally {
            //while loop
            while (i <= number) {
                num = num * i;
                i++;
            }
            System.out.println("The factorial of " + number + " is " + num);

            //for loop
//            for (i = 1; i <= number; i++){
//                num = num * i;
//          }
//            System.out.println("The factorial of " + number + " is " + num);


        }


    }
}
